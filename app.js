angular.module('cannasos', [
    'ngRoute'
]).config(function($routeProvider, $locationProvider) {
    $routeProvider.when('/', {
        templateUrl: 'templates/advices.html',
        controller: 'AdvicesCtrl',
        resolve: {
            advices: function(data) {
                return data.advices();
            },
            strains: function(data) {
                return data.strains();
            },
            strainCategories: function(data) {
                return data.strainCategories();
            }
        }
    }).when('/:adviceId', {
        templateUrl: 'templates/advice-details.html',
        controller: 'AdviceDetailsCtrl',
        resolve: {
            advice: function(data, $route) {
                return data.advice($route.current.params.adviceId);
            },
            strains: function(data) {
                return data.strains();
            },
            strainCategories: function(data) {
                return data.strainCategories();
            }
        }
    }).otherwise({
        redirectTo: '/'
    });
    $locationProvider.html5Mode(true);
}).run(function() {
}).factory('localStorageSvc', function() {
    return {
        removeItem: function(key) {
            localStorage.removeItem(key)
        },
        getItem: function(key) {
            return JSON.parse(localStorage.getItem(key));
        },
        setItem: function(key, value) {
            localStorage.setItem(key, JSON.stringify(value));
        }
    }
}).factory('utils', function($q, localStorageSvc) {
    function now() {
        return new Date().getTime();
    }

    return {
        indexBy: function(collection, key) {
            return collection.reduce(function(memo, item) {
                memo[item[key]] = item;
                return memo;
            }, {})
        },
        cache: function(cachedKey, call, timeout) {
            var deferred = $q.defer();
            var cachedItem = localStorageSvc.getItem(cachedKey);
            if (!cachedItem || now() > cachedItem.expireTime) {
                localStorageSvc.removeItem(cachedKey);
                call().then(function(data) {
                    localStorageSvc.setItem(cachedKey, {
                        expireTime: now() + timeout,
                        data: data
                    });
                    deferred.resolve(data);
                })
            } else {
                deferred.resolve(cachedItem.data);
            }
            return deferred.promise;
        },
        getAllPages: function(apiCall) {
            var deferred = $q.defer();
            var _getAllPages = function(i, allItems) {
                apiCall(i).then(function(itemsPage) {
                    if (itemsPage.data.length) {
                        _getAllPages(i + 1, allItems.concat(itemsPage.data));
                    } else {
                        deferred.resolve(allItems);
                    }
                });
            };
            _getAllPages(1, []);
            return deferred.promise;
        }
    }
}).factory('data', function(utils, api, $q) {
    var hour = 60 * 60 * 1000;

    return {
        advice: function(adviceId) {
            return api.advices({_id: adviceId});
        },
        advices: function() {
            return utils.getAllPages(function(pageNumber) {
                return api.advices({page: pageNumber, perPage: 100, fields: 'title,strain._id'});
            });
        },
        strains: function() {
            var deferred = $q.defer();
            utils.cache('strains', function() {
                return utils.getAllPages(function(pageNumber) {
                    return api.strains({page: pageNumber, perPage: 100, fields: 'title,category._id'});
                });
            }, hour).then(function(data) {
                deferred.resolve(utils.indexBy(data, '_id'));
            });
            return deferred.promise;
        },
        strainCategories: function() {
            var deferred = $q.defer();
            utils.cache('strainCategories', function() {
                return utils.getAllPages(function(pageNumber) {
                    return api.strainCategories({page: pageNumber, perPage: 100, fields: 'title'});
                });
            }, hour).then(function(data) {
                deferred.resolve(utils.indexBy(data, '_id'));
            });
            return deferred.promise;
        }
    }
}).factory('api', function($http) {
    return {
        //?_id=xxx
        //?page=1&perPage=10&fields=title,strain.title
        advices: function(params) {
            return $http.get('https://cannasos.com/api/advices', {params: params});
        },
        strains: function(params) {
            return $http.get('https://cannasos.com/api/strains', {params: params});
        },
        strainCategories: function(params) {
            return $http.get('https://cannasos.com/api/strainCategories', {params: params});
        }
    }
}).controller('AppCtrl', function($scope) {
    $scope.clearLocalCache = function() {
        localStorage.removeItem('strains');
        localStorage.removeItem('strainCategories');
    }
}).controller('AdvicesCtrl', function(advices, $scope, strains, strainCategories) {
    $scope.advices = advices;
    $scope.strains = strains;
    $scope.strainCategories = strainCategories;
}).controller('AdviceDetailsCtrl', function(advice, strains, strainCategories, $scope) {
    $scope.advice = advice.data[0];
    $scope.strains = strains;
    $scope.strainCategories = strainCategories;
});