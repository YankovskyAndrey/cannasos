var express = require('express');
var app = express();
var server = require('http').createServer(app);
var path = require('path');
var historyApiFallback = require('connect-history-api-fallback');
app.use(historyApiFallback);
var root = path.normalize(__dirname);
if (process.argv[2] == 'build') {
    app.use(express.static(path.join(root, 'dist')));
} else {
    app.use(express.static(root));
}
server.listen(8080, 'localhost', function () {
    console.log('Express server listening on %d', 8080)
});