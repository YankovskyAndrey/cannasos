Task https://db.tt/iIlry4zt

 Advices — таблица вида advice.title | strain.title | strainCategory.title;
 Advice details — страница одного advice (advice.title, strain.title, strainCategory.title).
Следует учесть, что данные в таблицах Strains и StrainCategorties меняются редко, поэтому
наиболее эффективным решение будет использование кеширования, например, в local storage.
Достойный внешний вид пользовательского интерфейса

Тестовое задание FrontEnd разработчика
Ориентировочное время выполнения — 3 часа
Задача
Разработать SPA-приложение, содержащее следующие страницы:
 Advices — таблица вида advice.title | strain.title | strainCategory.title;
 Advice details — страница одного advice (advice.title, strain.title, strainCategory.title).
Следует учесть, что данные в таблицах Strains и StrainCategorties меняются редко, поэтому
наиболее эффективным решение будет использование кеширования, например, в local storage.

Обязательные условия
1. Фреймворк — angular.js
2. Приложение должно быть работоспособным после минификации с включенным mangle
3. Доступ к ресурсам должен быть оформлен через сервис или ресурс
4. Использовать gulp/grunt для сборки
5. Валидная верстка с использованием директив angular.js

Желательные условия
1. Использовать ng-templates
2. Использовать ng-annotate
3. Использовать lodash/underscore, если необходимо
4. Достойный внешний вид пользовательского интерфейса
5. Использование LESS

Доступ к данным
Коллекция Url
Advices https://cannasos.com/api/advices
Strains https://cannasos.com/api/strains
StrainCategories https://cannasos.com/api/strainCategories
Примеры запросов
Тип Параметры url
По _id https://cannasos.com /api/advices?_id=xxx
Постраничный https://cannasos.com /api/advices?page=1&perPage=10
С указанием полей https://cannasos.com /api/advices?fields=title,strain.title
Комбинированный https://cannasos.com /api/advices?page=1&perPage=10&fields=title,strain.title