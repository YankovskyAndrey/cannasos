module.exports = function(grunt) {
    require('load-grunt-tasks')(grunt);

    grunt.initConfig({
        clean: {
            dist: ['.tmp', 'dist']
        },
        less: {
            dist: {
                files: {
                    '.tmp/app.css': 'app.less'
                }
            }
        },
        copy: {
            dist: {
                expand: true,
                dest: 'dist/',
                flatten: true,
                src: ['.tmp/app.css', 'index.html']
            }
        },
        useminPrepare: {
            dist: {
                src: 'index.html'
            }
        },
        ngtemplates: {
            dist: {
                src: 'templates/*.html',
                dest: 'dist/templates.js',
                options: {
                    module: 'cannasos',
                    usemin: 'app.js'
                }
            }
        },
        ngAnnotate: {
            dist: {
                files: {
                    '.tmp/concat/app.js': '.tmp/concat/app.js'
                }
            }
        },
        usemin: {
            html: ['dist/index.html']
        }
    });

    grunt.registerTask('default', [
        'clean',
        'less'
    ]);

    grunt.registerTask('build', [
        'clean',
        'less',
        'copy',
        'useminPrepare',
        'ngtemplates',
        'concat',
        'cssmin',
        'ngAnnotate',
        'uglify',
        'usemin'
    ]);
};