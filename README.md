Before using app execute `npm install` and `bower install`.

To build dev version and start server use `grunt` and then `node server.js`.

For production build use `grunt build` and `node server.js build`.